package com.demo.springboot.dto;

public class ResultDto {

    private Double x1;
    private Double x2;
    private Double delta;
    private Double p;
    private Double q;

    public ResultDto(){
        x1 = null;
        x2 = null;
        delta = null;

    }
    public ResultDto(Double x1, Double x2, Double delta, Double p, Double q) {
        this.x1 = x1;
        this.x2 = x2;
        this.delta = delta;
        this.p = p;
        this.q = q;
    }

    public Double getX1() {
        return x1;
    }

    public Double getX2() {
        return x2;
    }

    public Double getDelta() {
        return delta;
    }
    public Double getP() {
        return p;
    }
    public Double getQ() {
        return q;
    }
}


