package com.demo.springboot.rest;

public class DocumentDTO {

    private String x1;
    private String x2;
    private Double a;
    private Double b;
    private Double c;
    private Double y;

    public DocumentDTO() {
    }

    public DocumentDTO(String x1, String x2, Double a, Double b, Double c, Double y) {
        this.x1 = x1;
        this.x2 = x2;
        this.a = a;
        this.b = b;
        this.c = c;
        this.y = y;
    }



    public String getx1() {
        return x1;
    }

    public String getx2() {
        return x2;
    }

    public Double getA() {
        return a;
    }

    public Double getB() {
        return b;
    }

    public Double getC() {
        return c;
    }
}
